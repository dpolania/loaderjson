class loverspinner{
   
   constructor(object){
        this.createContainer(object);
    }
    //se encarga de crear el div donde se renderiza la animacion
    createContainer(object){
        var iDiv = document.createElement('div');
        iDiv.id = 'animation-lottie';
        iDiv.setAttribute("style", "position: fixed;height: 50%;width: 50%;left:25%;top:25%;")
        document.getElementById(object).appendChild(iDiv);
    }

    //funcion encargada de crear un animacion basica desde un json
    //Este recibe el id del div donde se quiere que se carge la animacion
    renderSpinner(){
        lottie.loadAnimation({
            container: document.getElementById('animation-lottie'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            path: 'animations/8278-graph.json'
          });
    }
    stopSpinner(){
        document.getElementById('animation-lottie').remove();
    }
}

var spinner = new loverspinner('op');
spinner.renderSpinner();